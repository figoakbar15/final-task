import React, {createContext, useState} from 'react';
import axios from 'axios';
import { View, StyleSheet, Button, Alert } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BASE_URL} from '../config'

export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [userInfo, setUserInfo] = useState({});

    const login = (service,username, password) => {
        setIsLoading(true);

        axios.post(`${BASE_URL}/login/token.php?service=${service}&username=${username}&password=${password}`, {
            service,
            username, 
            password
        }).then(res=>{
            let userInfo = res.data
            if(userInfo.error == "Invalid login, please try again"){
                Alert.alert(
                    "Login Failed",
                    "Incorrect Username or Password",
                    [
                      { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                  );
            }else{
                console.log(userInfo)
                setUserInfo(userInfo)
                AsyncStorage.setItem('userInfo', JSON.stringify(userInfo))
                setIsLoading(false)
            }

        }).catch(err=>{
            console.log(`login error ${err}`)
            setIsLoading(false)
        })
    }

    const logout = () => {
        AsyncStorage.removeItem('userInfo')
        setUserInfo({})
        setIsLoading(false)
        Alert.alert(
            "Logout",
            "Akun anda telah ter logout",
            [
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
          );
    }
    

    return(
        <AuthContext.Provider 
        value={{
            isLoading,
            userInfo,
            login,
            logout
            }}>
            {children}
        </AuthContext.Provider>
    );
};