import React, { useEffect, useState, useContext, Component, } from 'react';
import { Alert, Modal, Pressable, View, Text, Image, StyleSheet, ImageBackground, TouchableOpacity, FlatList, SafeAreaView, ScrollView} from 'react-native';
import AppLoading from 'expo-app-loading';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';

import axios from 'axios';
import {BASE_URL, BASE_URL_AFTER_LOGIN} from '../config'
import { AuthContext } from '../Auth/AuthContext';

import Ionicons from 'react-native-vector-icons/Ionicons';
import * as Progress from 'react-native-progress';
import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_300Light,
  Poppins_300Light_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_500Medium,
  Poppins_500Medium_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
  Poppins_700Bold,
  Poppins_700Bold_Italic,
  Poppins_800ExtraBold,
  Poppins_800ExtraBold_Italic,
  Poppins_900Black,
  Poppins_900Black_Italic,
} from '@expo-google-fonts/poppins';




export default function Matakuliah({route, navigation}){
    let [fontsLoaded] = useFonts({
        Poppins_100Thin,
        Poppins_100Thin_Italic,
        Poppins_200ExtraLight,
        Poppins_200ExtraLight_Italic,
        Poppins_300Light,
        Poppins_300Light_Italic,
        Poppins_400Regular,
        Poppins_400Regular_Italic,
        Poppins_500Medium,
        Poppins_500Medium_Italic,
        Poppins_600SemiBold,
        Poppins_600SemiBold_Italic,
        Poppins_700Bold,
        Poppins_700Bold_Italic,
        Poppins_800ExtraBold,
        Poppins_800ExtraBold_Italic,
        Poppins_900Black,
        Poppins_900Black_Italic,
      });


    const {userInfo} = useContext(AuthContext);
    const [items, setItems]=useState({});
    const [eventItems, seteventItems]=useState({});

    const [completionTugas, setCompletionTugas] = useState({})
    const [completionQuiz, setCompletionQuiz] = useState({})
    const [isCompletionTugas, setisCompletionTugas] = useState(false)
    const [isCompletionQuiz, setisCompletionQuiz] = useState(false)

    const [modalVisible, setModalVisible] = useState(false);
    const [isAgenda, setIsAgenda]=useState(false);
    const [time, setTime] = useState(new Date())
    
    const BASE_URL_COURSE = `${BASE_URL_AFTER_LOGIN}?wstoken=${userInfo.token}&moodlewsrestformat=json&courseid=${route.params.key.id}`

    const back=()=>{
        return(
            navigation.navigate("Calendar")
        )
    }

    const timestampConverter=(date)=>{
        if (date == null){
            return ""
        } else{
            var t = new Date( date*1000 )
            var formatted = (t.getFullYear()) 
            + '-' + ('0' + (t.getMonth() + 1) ).slice(-2)
            + '-' + ('0' + t.getDate()).slice(-2)
            
            return(formatted)
        }
        
    }

    const getTime=(date)=>{
        if (date == null){
            return ""
        } else{
            var t = new Date( date*1000 )
            var hours = t.getHours();
            var minutes = t.getMinutes();
            var newformat = t.getHours() >= 12 ? 'PM' : 'AM';  

            // Find current hour in AM-PM Format 
            hours = hours % 12;  

            // To display "0" as "12" 
            hours = hours ? hours : 12;  
            minutes = minutes < 10 ? '0' + minutes : minutes; 
            var formatted = (t.toString().split(' ')[0])
            + ' - ' + ('0' + t.getHours()).slice(-2)
            + ':' + ('0' + t.getMinutes()).slice(-2)
            + ' ' + newformat;

            return(formatted)
        }
    }

    const getCurrentDate=()=>{
        var date = new Date()
        var day = ('0' + (time.getDate())).slice(-2);
        var month = (time.getMonth() + 1);
        var year = time.getFullYear();
  
        return year + '-' + '0' + month + '-' + day;
    }

    const getCalendarEvent=(day)=>{
        const checkArray = []
        
        axios.get(`${BASE_URL_COURSE}&wsfunction=core_calendar_get_calendar_day_view&year=${day.year}&month=${day.month}&day=${day.day}`)
        .then(res=>{
            const data = (res.data)
            seteventItems(data)
            setModalVisible(!modalVisible);
            data.events.map((e) => {
                checkArray.push(e.timesort) 
            })
            if(!checkArray.length){
                setIsAgenda(false)
            }else{
                setIsAgenda(true) 
            }
        }).catch(e=>{
            console.log(`error ${e}`)

        })
        
    }

    function getWord(str) {
        const lastIndexOfSpace = str.lastIndexOf(' ');
      
        if (lastIndexOfSpace === -1) {
          return str;
        }
        
      
        return str.substring(0, lastIndexOfSpace);
    }
    
    
    const GetDetailMatakuliah=()=>{
        var completionTugasArray = []
        var completionQuizArray = []
        axios.get(`${BASE_URL_COURSE}&wsfunction=core_course_get_contents`)
        .then(res=>{
            const data = (res.data)
            setItems(data)
            Array.from(data).forEach((e) => {
                e.modules.forEach((module)=>{
                    if(module.modname == "assign"){
                        if(module.completiondata.timecompleted != 0){
                            completionTugasArray.push(module.name)
                        }
                    }else if(module.modname == "quiz"){
                        if(module.completiondata.timecompleted != 0){
                            completionQuizArray.push(module.name)
                        }
                    }
                })
            })
            setCompletionTugas(completionTugasArray)
            setCompletionQuiz(completionQuizArray)
        }).catch(e=>{
            console.log(`error ${e}`)

        })
    }
    

    let markedDay = {};

    Array.from(items).forEach((e) => {
        e.modules.forEach((module) => {
            module.dates.forEach((date) => { 
                if(date.label == "Due:" || date.label =="Closed:" || date.label =="Closes:"){
                    if(getCurrentDate() <= timestampConverter(date.timestamp)){
                        if(module.completiondata.timecompleted != 0){
                            markedDay[(timestampConverter(date.timestamp))]
                            = {
                                    selected: true,
                                    marked: true,
                                    selectedColor: "#00ff00",
                                }; 
                        }else if (module.completiondata.timecompleted == 0){
                            markedDay[(timestampConverter(date.timestamp))]
                        = {
                            selected: true,
                            marked: true,
                            selectedColor: "#EED202",
                        };
                        }
                    } else {
                        if(module.completiondata != null){
                            if(module.completiondata.timecompleted != 0){
                                markedDay[(timestampConverter(date.timestamp,0))]
                                = {
                                        selected: true,
                                        marked: true,
                                        selectedColor: "#00ff00",
                                    }; 
                            } else{
                                markedDay[(timestampConverter(date.timestamp,0))]
                                = {
                                    selected: true,
                                    marked: true,
                                    selectedColor: "#cc0000",
                                }; 
                            }
                        }
                    }
                }
            })
        })
    })


    const activityProgressTugas=()=>{
        var count = 0
        var total = 0
        Array.from(items).forEach((e) => {
            e.modules.forEach((module) => {
                if(module == null){
                    count = count + 0
                    total = total + 0
                }else{
                    if(module.completion == 0){
                        count = count + 0
                        total = total + 0
                    }else{
                        if(module.modname == 'assign'){
                            total = total + 1
                            if(module.completiondata.timecompleted != 0){
                                count = count + 1
                            }
                            else{
                                count = count + 0
                            }
                        }else{
                            total = total + 0
                            count = count + 0
                        }
                    }
                }
                
            })
        })
        let num = ((100*count)/total)/100
        let percent = num*100
        percent = percent.toFixed(1)
        return (percent)
    }

    const activityProgressQuiz=()=>{
        var count = 0
        var total = 0
        Array.from(items).forEach((e) => {
            e.modules.forEach((module) => {
                if(module == null){
                    count = count + 0
                    total = total + 0
                }else{
                    if(module.completion == 0){
                        count = count + 0
                        total = total + 0
                    }else{
                        if(module.modname == 'quiz'){
                            total = total + 1
                            if(module.completiondata.timecompleted != 0){
                                count = count + 1
                            }
                            else{
                                count = count + 0
                            }
                        }else{
                            total = total + 0
                            count = count + 0
                        }
                    }
                }
                
            })
        })
        let num = ((100*count)/total)/100
        let percent = num*100
        percent = percent.toFixed(1)
        return (percent)
    }

    const CheckCompletionTugas=(name, time)=>{ 
        var complete = false
        for(let i = 0; i < completionTugas.length; i++){
            const withoutLast2Words = name.split(' ').slice(0, -2).join(' ');
            if(completionTugas[i] == withoutLast2Words){
                complete = true
            }else{
                complete = false
            }
        }
        if(complete == true){
            return(
                <View style={styles.agendaDeadline}>
                    <Text style={styles.agendaDesc}>Deadline: </Text>
                    <Text style={styles.agendaDescComplete}>Sudah Dikerjakan</Text>
                </View>
            )
        }else if(getCurrentDate() > timestampConverter(time) && complete == false){
            return(
                <View style={styles.agendaDeadline}>
                    <Text style={styles.agendaDesc}>Deadline: </Text>
                    <Text style={styles.agendaDescFailed}>Sudah Terlewat</Text>
                </View>
            )
        }else{
            return(
            <View style={styles.agendaDeadline}>
                <Text style={styles.agendaDesc}>Deadline: </Text>
                <Text style={styles.agendaTime}>{getTime(time)}</Text>
            </View>
            )  
        }
    }

    const CheckCompletionQuiz=(name, time)=>{ 
        var complete = false
        for(let i = 0; i < completionQuiz.length; i++){
            if(completionQuiz[i] == getWord(name)){
                complete = true
            }else{
                complete = false
            }
        }
        if(complete == true){
            return(
                <View style={styles.agendaDeadline}>
                    <Text style={styles.agendaDesc}>Deadline: </Text>
                    <Text style={styles.agendaDescComplete}>Sudah Dikerjakan</Text>
                </View>
        )
        }else if(getCurrentDate() > timestampConverter(time) && complete == false){
            return(
                <View style={styles.agendaDeadline}>
                    <Text style={styles.agendaDesc}>Deadline: </Text>
                    <Text style={styles.agendaDescFailed}>Sudah Terlewat</Text>
                </View>
            )
        } else{
            return(
            <View style={styles.agendaDeadline}>
                <Text style={styles.agendaDesc}>Deadline: </Text>
                <Text style={styles.agendaTime}>{getTime(time)}</Text>
            </View>
            )  
        }
    }


    const agendaCheck=()=>{
        var completed = false
        if(isAgenda){
            return(
            eventItems?.events?.map((e,key) => { 
                if(e.modulename == "quiz"){
                    return(
                        <View key={key} style={styles.modalText}>
                            <Text style={styles.agendaName}>{getWord(e.name)}</Text>
                            {CheckCompletionQuiz(e.name, e.timesort)}
                        </View>
                    )
                }else if(e.modulename == "assign"){
                    const withoutLast2Words = e.name.split(' ').slice(0, -2).join(' ');
                    return(
                        <View key={key} style={styles.modalText}>
                            <Text style={styles.agendaName}>{withoutLast2Words}</Text>
                            {CheckCompletionTugas(e.name, e.timesort)}
                        </View>
                    )
                } 
                
                
            })
        )
        }else{
            return(
                <View style={styles.modalText}>
                    <Text style={styles.agendaName}>No agenda Today!</Text>
                </View>
            )
        }
    }

    useEffect(() => {
        GetDetailMatakuliah();
        const interval = setInterval(()=>{ 
            setTime(new Date()); 
         }, 1000);

         return () => {
            clearInterval(interval);
            };
    }, [])

    if (!fontsLoaded) {
        return <AppLoading />;
    }

    return(
        <View style={styles.container}>
            <Image 
            style={styles.header}
            source={require('../Assets/header_2.png')}
            />
                <View style={styles.matakuliahContainer}>
                <TouchableOpacity onPress={()=>back()}>
                    <Ionicons name={"close-outline"} color={'black'} style={styles.closeIcon}/>
                </TouchableOpacity>
                    <View style={styles.matakuliahHeader}>
                        <Text style={styles.matakuliah}>{route.params.key.fullname}</Text>
                    </View>
                    <View style={styles.calendarDesc}>
                            <View style={styles.redDesc}>
                                <View style={styles.redCircles} />
                                <Text style={styles.redText} >Terlewat</Text>
                            </View>
                            <View style={styles.yellowDesc}>
                                <View style={styles.yellowCircles} />
                                <Text style={styles.yellowText} >Belum Selesai</Text>
                            </View>
                            <View style={styles.greenDesc}>
                                <View style={styles.greenCircles} />
                                <Text style={styles.greenText} >Selesai</Text>
                            </View>  
                    </View>
                    <Calendar style={styles.calendar} onDayPress={day => {
                                            getCalendarEvent(day);
                                        }}
                            markingType={'multi-dot'}
                            markedDates={markedDay}/> 
                    
                    <View style={styles.centeredView}>
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => {
                            Alert.alert("Modal has been closed.");
                            setModalVisible(!modalVisible);
                            }}
                        >
                            

                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                <View style={styles.modalTitle}>
                                    <Text style={styles.titleAgenda}>Agenda on {eventItems.periodname}</Text>
                                </View>
                              
                                {agendaCheck()}
                                
                                
                                    <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setModalVisible(!modalVisible)}
                                    >
                                    <Text style={styles.textStyle}>Close</Text>
                                    </Pressable>
                                </View>
                            </View>
                        </Modal>
                        
                        
                    </View>
                    
                    <View style={styles.containerProgress}>
                            <View style={styles.progressMatakuliah}>
                            <Text style={styles.activityProgress}>
                                Progress aktivitas 
                            </Text>
                                <View style={styles.containerMatakuliah}>
                                    <Text style={styles.namaProgress}>
                                        Quiz
                                    </Text>
                                    <Text style={styles.progressActivity}>
                                        {activityProgressQuiz()} % Completed
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.progressMatakuliah}>
                                <View style={styles.containerMatakuliah}>
                                    <Text style={styles.namaProgress}>
                                        Tugas
                                    </Text>
                                    <Text style={styles.progressActivity}>
                                        {activityProgressTugas()} % Completed
                                    </Text>
                                </View>
                            </View>
                    </View>
                </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        width: '100%',
        maxHeight: '70%',
        height: '60%',
        position: 'absolute'
    },
    matakuliahContainer:{
        marginLeft: '10%',
        marginTop: '15%',
        marginRight: '10%'
    },

    closeIcon:{
        fontSize: 25,
        fontWeight: 'bold',
        color: 'white'
    },

    matakuliahHeader:{
        width: '100%',
        justifyContent: 'space-between'
    },

    matakuliah:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 24,
        color: 'white'
    },

    namaMatakuliah:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 14,
        color: 'black',
        marginLeft: '4%',
        marginRight: '4%',
        marginTop: '3%',
        marginBottom: '3%',
    },

    title:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 14,
        color: '#222222'
    },

    calendar:{
        borderRadius: 20,
        marginTop: '10%'
    },

    containerProgress:{
        marginTop: '10%'
    },

    containerMatakuliah:{
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        marginTop:'5%',
        backgroundColor:'#A02724'
        
    },
    namaProgress:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 14,
        color: 'white',
        marginLeft: '4%',
        marginRight: '4%',
        marginTop: '3%',
    },
    activityProgress:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 14,
    },
    progressActivity:{
        color: 'white',
        alignSelf: 'flex-end',
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 12,
        marginRight: '5%',
        marginBottom: '2%'
    },

    centeredView: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: 'rgba(100,100,100, 0.5)',
        width: '100%',
        height: '100%',
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      modalTitle:{
        alignItems : 'center',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: 'green'
      },
      titleAgenda:{
        color: 'white',
        margin: '2%',
        fontFamily: 'Poppins_600SemiBold',
      },
      agendaName:{
        fontFamily: 'Poppins_600SemiBold',
      },
      button: {
        borderRadius: 20,
        padding: 8,
        elevation: 2,
        marginHorizontal: '5%',
        marginBottom: '5%'
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginVertical: "5%",
        marginLeft: "5%",
        marginRight: "5%",
        textAlign: "center",
        
      },
      agendaDeadline:{
          justifyContent: 'space-between',
          flexDirection: 'row',
      },
      agendaTime:{
        fontFamily: 'Poppins_700Bold',
      },
      agendaDesc:{
          fontFamily: 'Poppins_500Medium'
      },

      agendaDescComplete:{
        fontFamily: 'Poppins_700Bold',
        color: 'green'
    },

        agendaDescFailed:{
            fontFamily: 'Poppins_700Bold',
            color: 'red'
        },

      calendarDesc:{
        alignItems: 'center',
        flexDirection:'row',
        marginTop: '15%'
      },
      redDesc:{
        flexDirection: 'row',
        alignItems: 'center'
      },
      redCircles:{
        height: 30,
        width: 30,
        borderRadius: 30,
        backgroundColor: "#cc0000"
      },
      redText:{
          marginLeft: '5%',
          fontFamily: 'Poppins_600SemiBold',
          fontSize: 12,
      },

      yellowDesc:{
        flexDirection: 'row',
        alignItems: 'center'
      },
      yellowCircles:{
        height: 30,
        width: 30,
        borderRadius: 30,
        backgroundColor: "#EED202"
      },
      yellowText:{
          marginLeft: '5%',
          fontFamily: 'Poppins_600SemiBold',
          fontSize: 12,
      },

      greenDesc:{
        flexDirection: 'row',
        alignItems: 'center'
      },
      greenCircles:{
        height: 30,
        width: 30,
        borderRadius: 30,
        backgroundColor: "#00ff00"
      },
      greenText:{
          marginLeft: '5%',
          fontFamily: 'Poppins_600SemiBold',
          fontSize: 12,
      },
})