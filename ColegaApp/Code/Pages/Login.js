import React, { useState, useContext, useEffect } from 'react';
import { View, Text, StyleSheet, ImageBackground, SafeAreaView, TextInput, Pressable, Image} from 'react-native';
import { AuthContext } from '../Auth/AuthContext';

import AppLoading from 'expo-app-loading';


import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_300Light,
  Poppins_300Light_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_500Medium,
  Poppins_500Medium_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
  Poppins_700Bold,
  Poppins_700Bold_Italic,
  Poppins_800ExtraBold,
  Poppins_800ExtraBold_Italic,
  Poppins_900Black,
  Poppins_900Black_Italic,
} from '@expo-google-fonts/poppins';
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';
import Spinner from 'react-native-loading-spinner-overlay/lib';

export default function Login({navigation}){
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [usernameError, setUsernameError] = useState("")
    const [passwordError, setPasswordError] = useState("")

    const {isLoading, login} = useContext (AuthContext);

    let [fontsLoaded] = useFonts({
        Poppins_100Thin,
        Poppins_100Thin_Italic,
        Poppins_200ExtraLight,
        Poppins_200ExtraLight_Italic,
        Poppins_300Light,
        Poppins_300Light_Italic,
        Poppins_400Regular,
        Poppins_400Regular_Italic,
        Poppins_500Medium,
        Poppins_500Medium_Italic,
        Poppins_600SemiBold,
        Poppins_600SemiBold_Italic,
        Poppins_700Bold,
        Poppins_700Bold_Italic,
        Poppins_800ExtraBold,
        Poppins_800ExtraBold_Italic,
        Poppins_900Black,
        Poppins_900Black_Italic,
      });

      const handleSubmit = () => {
        var usernameValid = false;
        if(username == null){
            setUsernameError("Username is required");
        }
        else if(username.indexOf(' ') >= 0){        
            setUsernameError('Username cannot contain spaces');                          
        }    
        else{
            setUsernameError("")
            usernameValid = true
        }
    
        var passwordValid = false;
        if(password == null){
            setPasswordError("Password is required");
        }             
        else if(password.indexOf(' ') >= 0){        
            setPasswordError('Password cannot contain spaces');                          
        }    
        else{
            setPasswordError("")
            passwordValid = true
        }        
    
        if(usernameValid && passwordValid){            
            login('moodle_mobile_app',username,password)
        }        
    
      }
    
      if (!fontsLoaded) {
        return <AppLoading />;
      }

     
    return(
        <View style={styles.container}>
            <ImageBackground source={require('../Assets/Login-Background.png')} resizeMode='cover' style={styles.backgroundImage}>
            
                <View style={styles.AppTitle}>
                <Image 
                        style={styles.Title}
                        source={require('../Assets/AppTitle.png')}
                    />
                </View>
                <View style={styles.Header}>
                    <View style={styles.Welcome}>
                        <Text style={styles.WelcomeText}>
                            Selamat Datang
                        </Text>
                    </View>
                    <View style={styles.WelcomeDesc}>
                        <Text style={styles.Desc}>
                        Colega akan menjadi teman belajar aktivitas perkuliahan anda.
                        </Text>
                    </View>
                </View>
                <View style={styles.LoginContainer}>
                    <Spinner visible={isLoading}/>
                    <Text style={styles.LoginTitle}>
                        Login
                    </Text>
                    <TextInput
                        style={styles.username}
                        placeholder="Username"
                        value={username}
                        onChangeText={(value)=>setUsername(value)}
                    />
                    {usernameError.length > 0 &&
                        <Text style={styles.usernameError}>{usernameError}</Text>
                    }
                    <TextInput
                        style={styles.password}
                        placeholder="Password"
                        value={password}
                        onChangeText={(value)=>setPassword(value)}
                        secureTextEntry
                    />
                    {passwordError.length > 0 &&
            
                        <Text style={styles.passwordError}>{passwordError}</Text>
                    }
                    <Pressable style={styles.button} onPress={() => handleSubmit()}>
                            <Text style={styles.loginButton}>Masuk</Text>
                    </Pressable>
                </View>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    backgroundImage: {
        flex: 1,
        justifyContent: "flex-start"
    },
    AppTitle: {
        marginTop: '20%',
    },
    Title:{
        alignSelf: 'center',
        width: '70%'
    },
    Header:{
        marginLeft: '10%',
        marginTop: '20%',
    },
    Welcome:{
        marginRight: '10%'
    },
    WelcomeText:{
        fontSize: 36,
        fontFamily: 'Poppins_700Bold',
        color:'white'
    },

    WelcomeDesc:{
        marginRight: '25%',
        marginTop: '5%'
    },
    Desc:{
        fontFamily: 'Poppins_400Regular',
        fontSize: 15,
        color: '#fff'
    },
    LoginContainer:{
        height: '100%',
        width: '100%',
        marginTop: '20%',
        borderColor: '#C3C3C3',
        backgroundColor: 'white',
        borderRadius: 30,
        paddingLeft: '10%',
        paddingTop: '10%',
        paddingRight:'10%',
        
    },
    LoginTitle:{
        fontFamily:'Poppins_600SemiBold',
        fontSize: 20,
        color: '#222222'
    },
    username:{
        height: 44,
        width: '100%',
        borderWidth: 1,
        padding: 10,
        borderColor: '#C3C3C3',
        borderRadius: 8,
        marginTop:'5%'
    },

    password:{
        height: 44,
        width: '100%',
        borderWidth: 1,
        padding: 10,
        borderColor: '#C3C3C3',
        borderRadius: 8,
        marginTop:'5%'
    },


    button:{
        backgroundColor: '#FF2D31',
        borderRadius: 8,
        elevation: 3,
        width: '100%',
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 29
    },

    loginButton:{
        color: 'white',
        fontFamily: 'Poppins_500Medium',
    },

    usernameError:{
        color: 'red',
        fontFamily: 'Poppins_500Medium',
        fontSize: 10
    },

    passwordError:{
        color: 'red',
        fontFamily: 'Poppins_500Medium',
        fontSize: 10
    },
});