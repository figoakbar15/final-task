import React, { useState, useEffect, useRef, useContext } from 'react';
import{ View, Text, Image, StyleSheet, 
        ImageBackground, TouchableOpacity, 
        FlatList, Modal, ScrollView, 
        Pressable, Switch} from 'react-native';

import AppLoading from 'expo-app-loading';
import axios from 'axios';
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';


import Ionicons from 'react-native-vector-icons/Ionicons';

import SelectDropdown from 'react-native-select-dropdown';

import { AuthContext } from '../Auth/AuthContext';
import {BASE_URL, BASE_URL_AFTER_LOGIN} from '../config';




import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_300Light,
  Poppins_300Light_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_500Medium,
  Poppins_500Medium_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
  Poppins_700Bold,
  Poppins_700Bold_Italic,
  Poppins_800ExtraBold,
  Poppins_800ExtraBold_Italic,
  Poppins_900Black,
  Poppins_900Black_Italic,
} from '@expo-google-fonts/poppins';


export default function Home({}){

    let [fontsLoaded] = useFonts({
        Poppins_100Thin,
        Poppins_100Thin_Italic,
        Poppins_200ExtraLight,
        Poppins_200ExtraLight_Italic,
        Poppins_300Light,
        Poppins_300Light_Italic,
        Poppins_400Regular,
        Poppins_400Regular_Italic,
        Poppins_500Medium,
        Poppins_500Medium_Italic,
        Poppins_600SemiBold,
        Poppins_600SemiBold_Italic,
        Poppins_700Bold,
        Poppins_700Bold_Italic,
        Poppins_800ExtraBold,
        Poppins_800ExtraBold_Italic,
        Poppins_900Black,
        Poppins_900Black_Italic,
      });

      const {userInfo} = useContext(AuthContext);
      const [user, setuser]=useState({});
      const [calendarItems, setCalendarItems]=useState({});
      const [dropdownSelected, setdropdownSelected]=useState(0);
      
      const [time, setTime] = useState(new Date())

      const [expoPushToken, setExpoPushToken] = useState('');
      const [notification, setNotification] = useState(false);
     
      const notificationListener = useRef();
      const responseListener = useRef();

    const [isEnabled, setIsEnabled] = useState(true);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    const [modalVisible, setModalVisible] = useState(false);


    
      Notifications.setNotificationHandler({
        handleNotification: async () => ({
          shouldShowAlert: true,
          shouldPlaySound: true,
          shouldSetBadge: true,
        }),
      });

//API Function
    const getDataCalendar=()=>{
        axios.get(`${BASE_URL_AFTER_LOGIN}?wstoken=${userInfo.token}&wsfunction=core_calendar_get_calendar_upcoming_view&moodlewsrestformat=json`)
        .then(res=>{
            const data = (res.data)
            setCalendarItems(data)
        }).catch(err=>{
            console.log(`error ${err}`)

        });
    }

    const GetDataUser=()=>{
        axios.get(`${BASE_URL}/webservice/rest/server.php?wstoken=${userInfo.token}&wsfunction=core_webservice_get_site_info&moodlewsrestformat=json`)
        .then(res=>{
            const data = (res.data)
            setuser(data)
        }).catch(err=>{
            console.log(`error ${err}`)

        })
    }

    const sleep = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds))
    }

    function getWord(str) {
        const lastIndexOfSpace = str.lastIndexOf(' ');
      
        if (lastIndexOfSpace === -1) {
          return str;
        }
        
      
        return str.substring(0, lastIndexOfSpace);
    }


//Get Time Function 
    const timestampConverter=(date,minDays)=>{
        if (date == null){
            return ""
        } else{
            var date = new Date(date*1000);
            var last = new Date(date.getTime() - (minDays * 24 * 60 * 60 * 1000));
            let timeStamp =  last.getTime() / 1000;
                        
            return timeStamp
        }
        
    }


    const dateConverter=(timestamp, minDays)=>{
        if (timestamp == null){
            return ""
        } else{
            let date = new Date(timestamp*1000);
            var last = new Date(date.getTime() - (minDays * 24 * 60 * 60 * 1000));
            var day =last.getDate();
            var month=last.getMonth()+1;
            var year=last.getFullYear();
            var hours = last.getHours();
            var minutes = last.getMinutes();
            var seconds = last.getSeconds();
            var milliSeconds = last.getMilliseconds();

            return (day+"/"+month+"/"+year+" "+hours+":"+minutes+":"+seconds);
        }

    };

    const getCurrentDate=()=>{
        let date = new Date();
        var day =time.getDate();
        var month=time.getMonth()+1;
        var year=time.getFullYear();
        var hours = time.getHours();
        var minutes = time.getMinutes();
        var seconds = time.getSeconds();
        
        return (day+"/"+month+"/"+year+" "+hours+":"+minutes+":"+seconds);
    }

    const getCurrentDayOnly=()=>{
        var day =time.getDate();
        var month=time.getMonth()+1;
        var year=time.getFullYear();

        return (day+"/"+month+"/"+year);
    }

    const dayOnlyConverter=(timestamp, minDays)=>{
        if (timestamp == null){
            return ""
        } else{
            let date = new Date(timestamp*1000);
            var last = new Date(date.getTime() - (minDays * 24 * 60 * 60 * 1000));
            var day =last.getDate();
            var month=last.getMonth()+1;
            var year=last.getFullYear();

            return (day+"/"+month+"/"+year);
        }

    };



    const getTime=(date)=>{
        if (date == null){
            return ""
        } else{
            var t = new Date( date*1000 )
            var hours = t.getHours();
            var minutes = t.getMinutes();
            var newformat = t.getHours() >= 12 ? 'PM' : 'AM';  

            // Find current hour in AM-PM Format 
            hours = hours % 12;  

            // To display "0" as "12" 
            hours = hours ? hours : 12;  
            minutes = minutes < 10 ? '0' + minutes : minutes; 
            var formatted = ('0' + t.getHours()).slice(-2)
            + ':' + ('0' + t.getMinutes()).slice(-2)
            + ' ' + newformat;

            return(formatted)
        }
    }


//Notification Function
    const schedulePushNotificationTugasH3=()=>{
        var dateString = 0;
        var notificationTitle
        calendarItems?.events?.map((e)=>{
            if(e.modulename == "assign"){
                if(getCurrentDate() == dateConverter(e.timesort,3)){
                    dateString = timestampConverter(e.timesort,3)
                }
            }
            if(dateString == timestampConverter(e.timesort,3)){
                notificationTitle = e.name.split(' ').slice(0, -2).join(' ');
            }
            
        })
        const trigger = new Date(dateString*1000);
        trigger.setSeconds(20)
        const id = Notifications.scheduleNotificationAsync({
            content: {
            title: notificationTitle+", 3 Hari lagi!",
            body: `Bagaimana harimu ${user.firstname}? Dikerjakan yuk`+notificationTitle+" nya agar tidak mepet dengan deadline" ,
            data: { data: 'goes here' },
            },
            trigger,
        });
        return id
    } 

    const schedulePushNotificationTugasH2=()=>{
        var dateString = 0;
        var notificationTitle
        calendarItems?.events?.map((e)=>{
            if(e.modulename == "assign"){
                if(getCurrentDate() == dateConverter(e.timesort,2)){
                    dateString = timestampConverter(e.timesort,2)
                }
            }
            if(dateString == timestampConverter(e.timesort,2)){
                notificationTitle = e.name.split(' ').slice(0, -2).join(' ');
            }
            
        })
        const trigger = new Date(dateString*1000);
        trigger.setSeconds(20)
       
        const id = Notifications.scheduleNotificationAsync({
            content: {
            title: notificationTitle+", 2 Hari lagi!",
            body: `Hallo ${user.firstname}, Deadline `+notificationTitle+' makin dekat nih! Yuk kita kerjakan tugasnya' ,
            data: { data: 'goes here' },
            },
            trigger,
        });
        return id
    } 

    const schedulePushNotificationTugasH1=()=>{
        var dateString = 0;
        var notificationTitle
        
        calendarItems?.events?.map((e)=>{
            if(e.modulename == "assign"){
                if(getCurrentDate() == dateConverter(e.timesort,1)){
                    dateString = timestampConverter(e.timesort,1)
                }
            }
            if(dateString == timestampConverter(e.timesort,1)){
                notificationTitle = e.name.split(' ').slice(0, -2).join(' ');
            }
            
        })
        
        const trigger = new Date(dateString*1000);
        trigger.setSeconds(20)
        console.log(trigger)
        const id = Notifications.scheduleNotificationAsync({
                content: {
                title: notificationTitle+", 1 Hari lagi!",
                body: `Semangat ${user.firstname}, Kerjakan `+notificationTitle+` nya Besok merupakan waktu Deadline kamu!` ,
                data: { data: 'goes here' },
                },
                trigger,
            });
        
        return id
    } 

    const schedulePushNotificationTugasH=()=>{
        var dateString = 0;
        var notificationTitle
        calendarItems?.events?.map((e)=>{
            if(e.modulename == "assign"){
                if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,0)){
                    dateString = timestampConverter(e.timesort,0)
                }
            }
            if(dateString == timestampConverter(e.timesort,0)){
                notificationTitle = e.name.split(' ').slice(0, -2).join(' ');
            }
            
        })
        const trigger = new Date(dateString*1000);
        trigger.setHours(0)
        trigger.setMinutes(0)
        trigger.setSeconds(10)
        const id = Notifications.scheduleNotificationAsync({
            content: {
            title: notificationTitle+", Deadlinenya hari ini!",
            body: `Ayo ${user.firstname} cepat kerjakan, Hari ini merupakan kesempatanmu untuk mengerjakan `+notificationTitle ,
            data: { data: 'goes here' },
            },
            trigger,
        });
        return id
    } 


    const schedulePushNotificationQuizH3=()=>{
        var dateString = 0;
        var notificationTitle
        calendarItems?.events?.map((e)=>{
            if(e.modulename == "quiz"){
                if(getCurrentDate() == dateConverter(e.timesort,3)){
                    dateString = timestampConverter(e.timesort,3)
                }
            }
            if(dateString == timestampConverter(e.timesort,3)){
                notificationTitle = getWord(e.name);
            }
            
        })
        const trigger = new Date(dateString*1000);
        trigger.setSeconds(20)
        const id = Notifications.scheduleNotificationAsync({
            content: {
            title: notificationTitle+", 3 Hari lagi!",
            body: `Bagaimana harimu ${user.firstname}? Dikerjakan yuk`+notificationTitle+" nya agar tidak mepet dengan deadline" ,
            data: { data: 'goes here' },
            },
            trigger,
        });
        return id
    } 


    const schedulePushNotificationQuizH2=()=>{
        var dateString = 0;
        var notificationTitle
        calendarItems?.events?.map((e)=>{
            if(e.modulename == "quiz"){
                if(getCurrentDate() == dateConverter(e.timesort,2)){
                    dateString = timestampConverter(e.timesort,2)
                }
            }
            if(dateString == timestampConverter(e.timesort,2)){
                notificationTitle = getWord(e.name);
            }
            
        })
        const trigger = new Date(dateString*1000);
        trigger.setSeconds(20)
        const id = Notifications.scheduleNotificationAsync({
            content: {
            title: notificationTitle+", 2 Hari lagi!",
            body: `Hallo ${user.firstname}, Deadline `+notificationTitle+' makin dekat nih! Yuk kita kerjakan tugasnya' ,
            data: { data: 'goes here' },
            },
            trigger,
        });
        return id
    } 

    
    const schedulePushNotificationQuizH1=()=>{
        var dateString = 0;
        var notificationTitle
        calendarItems?.events?.map((e)=>{
            if(e.modulename == "quiz"){
                if(getCurrentDate() == dateConverter(e.timesort,1)){
                    dateString = timestampConverter(e.timesort,1)
                }
            }
            if(dateString == timestampConverter(e.timesort,1)){
                notificationTitle = getWord(e.name);
            }
            
        })
        const trigger = new Date(dateString*1000);
        trigger.setSeconds(20)
        const id = Notifications.scheduleNotificationAsync({
            content: {
            title: notificationTitle+", 1 Hari lagi!",
            body: `Semangat ${user.firstname}, Kerjakan `+notificationTitle+` nya Besok merupakan waktu Deadline kamu!` ,
            data: { data: 'goes here' },
            },
            trigger,
        });
        return id
    } 

    
    const schedulePushNotificationQuizH=()=>{
        var dateString = 0;
        var notificationTitle
        calendarItems?.events?.map((e)=>{
            if(e.modulename == "quiz"){
                if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,0)){
                    dateString = timestampConverter(e.timesort,0)
                }
            }
            if(dateString == timestampConverter(e.timesort,0)){
                notificationTitle = e.name.split(' ').slice(0, -2).join(' ');
            }
            
        })
        const trigger = new Date(dateString*1000);
        const id = Notifications.scheduleNotificationAsync({
            content: {
            title: notificationTitle+", Deadlinenya hari ini!",
            body: `Ayo ${user.firstname} cepat kerjakan, Hari ini merupakan kesempatanmu untuk mengerjakan `+notificationTitle ,
            data: { data: 'goes here' },
            },
            trigger,
        });
        return id
    } 
    
    async function registerForPushNotificationsAsync() {
        let token;

        if (Device.isDevice) {
          const { status: existingStatus } = await Notifications.getPermissionsAsync();
          let finalStatus = existingStatus;
          if (existingStatus !== 'granted') {
            const { status } = await Notifications.requestPermissionsAsync();
            finalStatus = status;
          }
          if (finalStatus !== 'granted') {
            alert('Failed to get push token for push notification!');
            return;
          }
          token = (await Notifications.getExpoPushTokenAsync()).data;
          console.log(token);
        } else {
          alert('Must use physical device for Push Notifications');
        }
      
        if (Platform.OS === 'android') {
          Notifications.setNotificationChannelAsync('default', {
            name: 'default',
            importance: Notifications.AndroidImportance.MAX,
            vibrationPattern: [0, 250, 250, 250],
            lightColor: '#FF231F7C',
          });
        }
        return token;
    }

    
    const isEnableSwitch=()=>{
        const days = ["1 Hari", "2 Hari", "3 Hari"]
        if(!isEnabled){
            Notifications.cancelAllScheduledNotificationsAsync();
            return(<View style={styles.switchDropdown}>
                        <Text style={styles.disabledText}>Notifikasi Nonaktif</Text>
                    </View>
            )
            
        }else{
            return(
                <View style={styles.switchDropdown}>
                    <Text style={styles.enabledText}>Pilih durasi notifikasi sebelum deadline</Text>
                    <SelectDropdown
                        data={days}
                        style={styles.dropdown}
                        onSelect={(selectedItem, index) => {
                            console.log(selectedItem, index)
                            if(index == 0){
                                setdropdownSelected(1)
                            }
                            else if(index == 1){
                                setdropdownSelected(2)
                            }else if(index == 2){
                                setdropdownSelected(3)
                            }else{
                                setdropdownSelected(0)
                            }
                        }}
                        buttonTextAfterSelection={(selectedItem, index) => {
                            // text represented after item is selected
                            return selectedItem
                        }}
                        rowTextForSelection={(item, index) => {
                            // text represented for each item in dropdown
                            return item
                        }}
                        defaultButtonText={"Pilih Hari"}
                    />
                </View>
            )
        }
    }

    const notificationScheduling=()=>{
        if(dropdownSelected == 1){
            schedulePushNotificationTugasH1();
            schedulePushNotificationTugasH();

            schedulePushNotificationQuizH1();
            schedulePushNotificationQuizH();
            console.log("h-1")
        }else if(dropdownSelected == 2){
            schedulePushNotificationTugasH2();
            schedulePushNotificationTugasH1();
            schedulePushNotificationTugasH();

            schedulePushNotificationQuizH2();
            schedulePushNotificationQuizH1();
            schedulePushNotificationQuizH();
            console.log("h-2")
        }else if(dropdownSelected == 3){
            schedulePushNotificationTugasH3();
            schedulePushNotificationTugasH2();
            schedulePushNotificationTugasH1();
            schedulePushNotificationTugasH();

            schedulePushNotificationQuizH3();
            schedulePushNotificationQuizH2();
            schedulePushNotificationQuizH1();
            schedulePushNotificationQuizH();
            console.log("h-3")
        }else{
            schedulePushNotificationTugasH3();
            schedulePushNotificationTugasH2();
            schedulePushNotificationTugasH1();
            schedulePushNotificationTugasH();

            schedulePushNotificationQuizH3();
            schedulePushNotificationQuizH2();
            schedulePushNotificationQuizH1();
            schedulePushNotificationQuizH();
            console.log("Activate")
        }
    }

    async function cancelNotification(notifId){
        await Notifications.cancelScheduledNotificationAsync(notifId);
    }


//Reminder Function
    const reminderH3=()=>{
        var dateString = 0
        var desc
        var activityArray = []
        calendarItems?.events?.map((e)=>{
            if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,3)){
                dateString =  dayOnlyConverter(e.timesort,3)
                activityArray.push(e.timesort)
            }
            
        })
        var countArray = activityArray.length
        if(dateString == getCurrentDayOnly()){
            return(
                <View style={styles.matakuliah}>
                    <View style={styles.containerMatakuliah}>
                        <Text style={styles.reminderDesc}>
                            Terdapat {countArray} Aktivitas perkuliahan deadline dalam 3 hari 
                        </Text>
                    </View>
                    {calendarItems?.events?.map((e,key)=>{
                        if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,3)){
                            if(e.modulename == "assign"){
                                desc = e.name.split(' ').slice(0, -2).join(' ');
                            }else if(e.modulename == "quiz"){
                                desc = getWord(e.name)
                            }
                            return(
                                <View key={key} style={styles.containerMatakuliah}>
                                    <View style={styles.reminderMatakuliah}>
                                        <Text style={styles.reminderTitle}>
                                            {desc}
                                        </Text>
                                        <View style={styles.reminderDeadline}>
                                            <Text style={styles.namaMatakuliahReminder}>
                                                {e.course.shortname}
                                            </Text>
                                            <Text style={styles.reminderTime}>Deadline: {getTime(e.timesort)}</Text>
                                        </View>
                                        
                                    </View>
                                    
                                </View>
                            )
                        }
                        
                    })}
                </View>
            )
        }
    }
   
    const reminderH2=()=>{
        var dateString = 0
        var desc
        var activityArray = []
        calendarItems?.events?.map((e)=>{
            if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,2)){
                dateString =  dayOnlyConverter(e.timesort,2)
                activityArray.push(e.timesort)
            }
            
        })
        var countArray = activityArray.length
        if(dateString == getCurrentDayOnly()){
            return(
                <View style={styles.matakuliah}>
                    <View style={styles.containerMatakuliah}>
                        <Text style={styles.reminderDesc}>
                        Terdapat {countArray} Aktivitas perkuliahan deadline dalam 2 hari 
                        </Text>
                    </View>
                    {calendarItems?.events?.map((e,key)=>{
                        if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,2)){
                            if(e.modulename == "assign"){
                                desc = e.name.split(' ').slice(0, -2).join(' ');
                            }else if(e.modulename == "quiz"){
                                desc = getWord(e.name)
                            }
                            return(
                                <View key={key} style={styles.containerMatakuliah}>
                                    <View style={styles.reminderMatakuliah}>
                                        <Text style={styles.reminderTitle}>
                                            {desc}
                                        </Text>
                                        <View style={styles.reminderDeadline}>
                                            <Text style={styles.namaMatakuliahReminder}>
                                                {e.course.shortname}
                                            </Text>
                                            <Text style={styles.reminderTime}>Deadline: {getTime(e.timesort)}</Text>
                                        </View>
                                        
                                    </View>
                                    
                                </View>
                            )
                        }
                        
                    })}
                </View>
            )
        }
    }

    const reminderH1=()=>{
        var dateString = 0
        var desc
        var activityArray = []
        calendarItems?.events?.map((e)=>{
            if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,1)){
                dateString =  dayOnlyConverter(e.timesort,1)
                activityArray.push(e.timesort)
            }
            
        })
        var countArray = activityArray.length
        if(dateString == getCurrentDayOnly()){
            return(
                <View style={styles.matakuliah}>
                    <View style={styles.containerMatakuliah}>
                        <Text style={styles.reminderDesc}>
                            Terdapat {countArray} Aktivitas perkuliahan yang besok merupakan deadlinenya 
                        </Text>
                    </View>
                    {calendarItems?.events?.map((e,key)=>{
                        if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,1)){
                            if(e.modulename == "assign"){
                                desc = e.name.split(' ').slice(0, -2).join(' ');
                            }else if(e.modulename == "quiz"){
                                desc = getWord(e.name)
                            }
                            return(
                                <View key={key} style={styles.containerMatakuliah}>
                                    <View style={styles.reminderMatakuliah}>
                                        <Text style={styles.reminderTitle}>
                                            {desc}
                                        </Text>
                                        <View style={styles.reminderDeadline}>
                                            <Text style={styles.namaMatakuliahReminder}>
                                                {e.course.shortname}
                                            </Text>
                                            <Text style={styles.reminderTime}>Deadline: {getTime(e.timesort)}</Text>
                                        </View>
                                        
                                    </View>
                                    
                                </View>
                            )
                        }
                        
                    })}
                </View>
            )
        }
    }

    const reminderH=()=>{
        var dateString = 0
        var desc
        var activityArray = []
        calendarItems?.events?.map((e)=>{
            if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,0)){
                dateString =  dayOnlyConverter(e.timesort,0)
                activityArray.push(e.timesort)
            }
            
        })
        var countArray = activityArray.length
        if(dateString == getCurrentDayOnly()){
            return(
                <View style={styles.matakuliah}>
                    <View style={styles.containerMatakuliah}>
                        <Text style={styles.reminderDesc}>
                        Terdapat {countArray} Aktivitas perkuliahan deadline pada hari ini
                        </Text>
                    </View>
                    {calendarItems?.events?.map((e,key)=>{
                        if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,0)){
                            if(e.modulename == "assign"){
                                desc = e.name.split(' ').slice(0, -2).join(' ');
                            }else if(e.modulename == "quiz"){
                                desc = getWord(e.name)
                            }
                            return(
                                <View key={key} style={styles.containerMatakuliah}>
                                    <View style={styles.reminderMatakuliah}>
                                        <Text style={styles.reminderTitle}>
                                            {desc}
                                        </Text>
                                        <View style={styles.reminderDeadline}>
                                            <Text style={styles.namaMatakuliahReminder}>
                                                {e.course.shortname}
                                            </Text>
                                            <Text style={styles.reminderTime}>Deadline: {getTime(e.timesort)}</Text>
                                        </View>
                                        
                                    </View>
                                    
                                </View>
                            )
                        }
                        
                    })}
                </View>
            )
        }
    }

    const reminderCheck=()=>{
        if(reminderH() == null && reminderH1() == null && reminderH2() == null && reminderH3() == null){
            return(
                <View style={styles.containerMatakuliah}>
                    <Text style={styles.reminderDesc}>
                        Tidak ada aktivitas perkuliahan yang mendekati deadline hari ini
                    </Text>
                </View>
            )
        }
    } 

    const pedagogicalReminder=()=>{
        var deadlineMinDays = 0
        var time
        var deadlineArray = []
        calendarItems?.events?.map((e)=>{
            if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,0)){
                deadlineArray.push(e.timesort)
                time = getTime(e.timesort)
            }else if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,1)){
                deadlineArray.push(e.timesort)
            }else if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,2)){
                deadlineArray.push(e.timesort)
            }
            else if(getCurrentDayOnly() == dayOnlyConverter(e.timesort,3)){
                deadlineArray.push(e.timesort)
            }
            
        })
        
        deadlineArray.sort(function(a, b) {
        return a - b;
        });

        for(var i = 0; i <= deadlineArray.length; i++){
            if(getCurrentDayOnly() == dayOnlyConverter(deadlineArray[i],0)){
                deadlineMinDays = 1
                break;
            }else if(getCurrentDayOnly() == dayOnlyConverter(deadlineArray[i],1)){
                deadlineMinDays = 2
                break;
            }else if(getCurrentDayOnly() == dayOnlyConverter(deadlineArray[i],2)){
                deadlineMinDays = 3
                break;
            }else if(getCurrentDayOnly() == dayOnlyConverter(deadlineArray[i],3)){
                deadlineMinDays = 4
                break;
            }
        }
        if(deadlineMinDays == 1){
            return(
                <View style={styles.memo}>
                <View style={styles.agentChatContainer}>
                    <View style={styles.agentChat}>
                        <Text style={styles.chatText}>
                            Cepat {user.firstname}!!! Hari ini merupakan kesempatan terakhir {user.firstname} untuk mengerjakan tugas.
                            deadlinenya pada pukul {time}
                        </Text>
                    </View>
                </View>
                <Image 
                    style={styles.memotutor}
                    source={require("../Assets/colega_h.png")}
                />
            </View>
            )
            
        }else if(deadlineMinDays == 2){
            return(<View style={styles.memo}>
                <View style={styles.agentChatContainer}>
                    <View style={styles.agentChat}>
                        <Text style={styles.chatText}>
                            Hati-hati {user.firstname} deadline tugas mu makin dekat! Yuk dikerjakan
                            jangan sampai terlewat
                        </Text>
                    </View>
                </View>
                <Image 
                    style={styles.memotutor}
                    source={require("../Assets/colega_h-1.png")}
                />
            </View>)
            
        }else if(deadlineMinDays == 3){
            return(
                <View style={styles.memo}>
                <View style={styles.agentChatContainer}>
                    <View style={styles.agentChat}>
                        <Text style={styles.chatText}>
                            Semangat {user.firstname}! dikerjakan yuk tugasnya agar
                            hasil dari tugas {user.firstname} lebih maksimal
                        </Text>
                    </View>
                </View>
                <Image 
                    style={styles.memotutor}
                    source={require("../Assets/colega_h-2.png")}
                />
            </View>
            )
            
        } else if(deadlineMinDays == 4){
            return(
                <View style={styles.memo}>
                <View style={styles.agentChatContainer}>
                    <View style={styles.agentChat}>
                        <Text style={styles.chatText}>
                            Yuk {user.firstname}, jangan diundur tugasnya agar bisa santai kaya 
                            colega!
                        </Text>
                    </View>
                </View>
                <Image 
                    style={styles.memotutor}
                    source={require("../Assets/colega_h-3.png")}
                />
            </View>
            )
            
        }else{
            return(
                <View style={styles.memo}>
                <View style={styles.agentChatContainer}>
                    <View style={styles.agentChat}>
                        <Text style={styles.chatText}>
                            Nama saya colega! Bagaimana kabar mu hari ini? Jangan lupa di cek yuk kalendernya agar tidak ada
                            aktivitas yang terlewat
                        </Text>
                    </View>
                </View>
                <Image 
                    style={styles.memotutor}
                    source={require("../Assets/colega.png")}
                />
            </View>
            )
        }
    }
    

    notificationScheduling()
    
    useEffect(() => {  
        const interval = setInterval(()=>{ 
            setTime(new Date()); 
            getDataCalendar();
            
         }, 1000);
        
         
         GetDataUser();
         
        registerForPushNotificationsAsync().then(token => setExpoPushToken(token)); 
     
        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
             setNotification(notification);
        });
 
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
            console.log(response);
        });
            

        return () => {
        clearInterval(interval);
        Notifications.removeNotificationSubscription(notificationListener.current);
        Notifications.removeNotificationSubscription(responseListener.current);
        };

    }, []);

    
      if (!fontsLoaded) {
        return <AppLoading />;
      }
    return(
    <ScrollView>
        <View style={styles.container}>
        <Image 
            style={styles.header}
            source={require("../Assets/header.png")}
        />
                <View style={styles.homeContainer}>
                    <View style={styles.homeHeader}>
                        <View style={styles.welcoming}>
                            <Text style={styles.hi}>Halo,</Text>
                            <Text style={styles.name}>{user.firstname}!</Text>
                        </View>
                        <View style={styles.profile}>
                            <TouchableOpacity onPress={()=>setModalVisible(!modalVisible)}>
                                <Ionicons name={'md-settings'} size={32} color={'white'} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {pedagogicalReminder()}
                    <View style={styles.mataKuliahTitle}>
                        <Text style={styles.title}>Reminder</Text>
                    </View>
                        <View style={styles.matakuliah}>
                            {reminderCheck()}
                            {reminderH()}
                            {reminderH1()}
                            {reminderH2()}
                            {reminderH3()}
                        </View>
                    
                </View>
                <Modal
                            animationType="fade"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => {
                            setModalVisible(!modalVisible);
                            }}
                        >
                            

                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                <View style={styles.modalTitle}>
                                    <Text style={styles.titleAgenda}>Reminder Settings</Text>
                                </View>
                                <View style={styles.reminderSettings}>
                                    <Text style={styles.enableText}>
                                        Aktifkan Notifikasi
                                    </Text>
                                    <Switch
                                        trackColor={{ false: "#767577", true: '#A02724' }}
                                        thumbColor={isEnabled ? "#f4f3f4" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={toggleSwitch}
                                        value={isEnabled}
                                        style={styles.switch}
                                    />
                                </View>
                                
                                {isEnableSwitch()}
                                
                                
                                    <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setModalVisible(!modalVisible)}
                                    >
                                    <Text style={styles.textStyle}>Close</Text>
                                    </Pressable>
                                </View>
                            </View>
                        </Modal>
        </View>
    </ScrollView>
    )
}



const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    header: {
        width: '100%',
        minHeight: '50%',
        position: 'absolute'
    },
    homeContainer:{
        marginLeft: '10%',
        marginTop: '15%',
        marginRight: '10%'
    },
    homeHeader:{
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between'
    },
    welcoming:{
        alignSelf: 'flex-start',
        flexDirection: 'row',
    },
    hi:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 24,
        marginRight: '2%',
        color: 'white'
    },
    name:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 24,
        marginRight: '2%',
        color:'white'
    },
    memo:{
        marginTop: '15%',
        flexDirection: 'row',
    },

    agentChatContainer:{
        width: '50%',
    },

    agentChat:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        marginTop:'5%',
        backgroundColor:'#A02724',
       
    },
    chatText:{
        color: 'white',
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 10,
        marginHorizontal: '5%',
        marginVertical: '4%',
        lineHeight: 18
    },
    mataKuliahTitle:{
        alignItems: 'center',
        marginTop: '15%',
    },
    title:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 16,
        color: '#A02724'
    },
    matakuliah:{
        marginBottom: '5%'
    },  
    containerMatakuliah:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        marginTop:'5%',
        backgroundColor:'#A02724',
        
        
    },
    reminderDesc:{
        fontFamily: 'Poppins_300Light',
        fontSize: 14,
        color: 'white',
        marginLeft: '4%',
        marginRight: '4%',
        marginTop: '5%',
        marginBottom: '5%',
    },

    reminderMatakuliah:{
        marginLeft: '4%',
        marginRight: '4%',
        marginTop: '5%',
        marginBottom: '5%',
    },

    reminderTitle:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 14,
        color: 'white',
       
    },
    namaMatakuliahReminder:{
        fontFamily: "Poppins_400Regular",
        fontSize: 12,
        color: 'white',
        
    },
    reminderDeadline:{
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    reminderTime:{
        fontFamily: "Poppins_700Bold",
        fontSize: 12,
        color: 'white',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: 'rgba(100,100,100, 0.5)',
        width: '100%',
        height: '100%',
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      modalTitle:{
        alignItems : 'center',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: '#A02724'
      },
      titleAgenda:{
        color: 'white',
        margin: '2%',
        fontFamily: 'Poppins_600SemiBold',
      },
      agendaName:{
        fontFamily: 'Poppins_600SemiBold',
      },
      button: {
        borderRadius: 20,
        padding: 8,
        elevation: 2,
        marginHorizontal: '5%',
        marginBottom: '5%'
      },
      
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginVertical: "5%",
        marginLeft: "5%",
        marginRight: "5%",
        textAlign: "center",
        
      },
      reminderSettings:{
          flexDirection: 'row',
          marginHorizontal: '10%',
          alignItems: 'center'
      },
      enableText:{
        fontFamily: 'Poppins_600SemiBold',
      },
      
      switchDropdown:{
          marginVertical: '5%',
          marginHorizontal: '10%',
          alignItems: 'center'
      },
      disabledText:{
          fontFamily: 'Poppins_600SemiBold',
          color: '#ababab',
          textAlign: 'center'
      },
      enabledText:{
        fontFamily: 'Poppins_500Medium',
        textAlign: 'center',
        paddingBottom:'5%',
        color: '#3a3a3a',
    },
})