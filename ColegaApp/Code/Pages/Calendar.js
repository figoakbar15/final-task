import React, { useEffect, useState, useContext } from 'react';
import { View, Text, Image, StyleSheet, ImageBackground, TouchableOpacity, FlatList, SafeAreaView, ScrollView} from 'react-native';
import AppLoading from 'expo-app-loading';
import axios from 'axios';
import * as Progress from 'react-native-progress';
import {BASE_URL, BASE_URL_AFTER_LOGIN} from '../config'

import { AuthContext } from '../Auth/AuthContext';

import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_300Light,
  Poppins_300Light_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_500Medium,
  Poppins_500Medium_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
  Poppins_700Bold,
  Poppins_700Bold_Italic,
  Poppins_800ExtraBold,
  Poppins_800ExtraBold_Italic,
  Poppins_900Black,
  Poppins_900Black_Italic,
} from '@expo-google-fonts/poppins';

export default function Calendar({route, navigation}){
    let [fontsLoaded] = useFonts({
        Poppins_100Thin,
        Poppins_100Thin_Italic,
        Poppins_200ExtraLight,
        Poppins_200ExtraLight_Italic,
        Poppins_300Light,
        Poppins_300Light_Italic,
        Poppins_400Regular,
        Poppins_400Regular_Italic,
        Poppins_500Medium,
        Poppins_500Medium_Italic,
        Poppins_600SemiBold,
        Poppins_600SemiBold_Italic,
        Poppins_700Bold,
        Poppins_700Bold_Italic,
        Poppins_800ExtraBold,
        Poppins_800ExtraBold_Italic,
        Poppins_900Black,
        Poppins_900Black_Italic,
      });

      const {userInfo} = useContext(AuthContext)
      const [items, setItems]=useState({});
      const [user, setuser]=useState({});

    
    const GetDataMatakuliah=()=>{
        axios.get(`${BASE_URL_AFTER_LOGIN}?wstoken=${userInfo.token}&wsfunction=core_webservice_get_site_info&moodlewsrestformat=json`)
        .then(res=>{
            const data = (res.data)
            setuser(data)
            axios.get(`${BASE_URL_AFTER_LOGIN}?wstoken=${userInfo.token}&wsfunction=core_enrol_get_users_courses&moodlewsrestformat=json&userid=${data.userid}`)
            .then(res=>{
                const data = (res.data)
                setItems(data)
            }).catch(err=>{
                console.log(`error ${e}`)

            })
        }).catch(err=>{
            console.log(`error ${e}`)

        })
    }

    const onSelectItem=(item)=>{
        return(
            navigation.navigate("Matakuliah",{
                screen:'Matakuliah',
                key: item,
            })
            )
    }

    const progressMatakuliah=(progress)=>{
        if(progress == null){
            return(
                progress = 0
            )
        } else{
            return(
                progress/100
            )
        }
    }

    useEffect(() => {
        GetDataMatakuliah()
    }, [])

    if (!fontsLoaded) {
        return <AppLoading />;
    }

    return(
        <View style={styles.container}>
            <Image 
            style={styles.header}
            source={require('../Assets/header_2.png')}
            />
            <View style={styles.calendarHeader}>
                        <Text style={styles.calendar}>Calendar</Text>
                        <Image 
                            style={styles.calendarImg}
                            source={require('../Assets/calendar.png')}
                        />
            </View> 
                <View style={styles.calendarContainer}>
                    <View style={styles.mataKuliahTitle}>
                        <Text style={styles.title}>Mata kuliah yang sedang diikuti</Text>
                    </View>
                    <SafeAreaView>
                    <FlatList
                        data={items}
                        keyExtractor={(item, index) => `${item.id}-${index}`}
                        renderItem={({item}) =>{
                        return (
                        <TouchableOpacity onPress={()=>onSelectItem(item)}>
                            <View style={styles.matakuliah}>
                                <View style={styles.containerMatakuliah}>
                                    <Text style={styles.namaMatakuliah}>
                                    {item.fullname}
                                    </Text>
                                    <Progress.Bar color='white' progress={progressMatakuliah(item.progress)} height={8} width={260} style={styles.progress}/>
                                    <Text style={styles.progressMatakuliah}>
                                        {item.progress.toFixed(1)} % Completed
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        )
                        }} />
                    </SafeAreaView>
                </View>
        </View>  
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        width: '100%',
        maxHeight: '70%',
        height: '60%',
        position: 'absolute'
    },
    calendarContainer:{
        marginLeft: '10%',
        marginRight: '10%'
    },
    calendarHeader:{
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginLeft: '10%',
    },

    calendar:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 24,
        color: 'white',
        marginTop: '15%',
    },
    calendarImg:{
        marginRight: '15%',
        marginTop: '5%',
    },
    mataKuliahTitle:{
        marginTop: '30%',
    },
    title:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 14,
        color: '#222222'
    },
    containerMatakuliah:{
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        marginTop:'5%',
        backgroundColor:'#A02724'
        
    },
    namaMatakuliah:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 14,
        color: 'white',
        marginLeft: '4%',
        marginRight: '4%',
        marginTop: '3%',
        marginBottom: '3%',
    },

    progressMatakuliah:{
        color: 'white',
        alignSelf: 'flex-end',
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 14,
        marginRight: '5%',
        marginBottom: '3%'
    },

    progress:{
        marginLeft: '4%',
        marginBottom: '5%',
        borderColor: '#A02724',
    }
})