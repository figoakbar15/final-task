import React, { useState, useContext } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';


import Login from '../Pages/Login';
import Home from '../Pages/Home';
import Profile from '../Pages/Profile';
import Calendar from '../Pages/Calendar';
import Matakuliah from '../Pages/Matakuliah';


import { AuthContext } from '../Auth/AuthContext';


import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

export default function Router() {
    const {userInfo} = useContext(AuthContext)
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                {userInfo.token ? (
                <Stack.Group>
                    <Stack.Screen name="MainApp" component={MainApp} />
                    <Stack.Screen name="Matakuliah" component={Matakuliah} />
                </Stack.Group>
                ) : (
                    <>
                        <Stack.Screen name="Login" component={Login} headerShown={false}/>
                        <Stack.Screen name="Profile" component={Profile} headerShown={false}/>
                        <Stack.Screen name="Calendar" component={Calendar} headerShown={false}/>
                    </>
                )}
            
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp =()=>{
    return(
        <Tab.Navigator screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = focused
                  ? 'md-home'
                  : 'md-home';
              } else if (route.name === 'Calendar') {
                iconName = focused ? 'md-calendar' : 'md-calendar';
              }else if (route.name === 'Profile') {
                iconName = focused ? 'md-person-circle' : 'md-person-circle';
              }
  
              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: 'tomato',
            tabBarInactiveTintColor: 'gray',
          })} >
            <Tab.Screen name="Home" component={Home} options={{headerShown: false}}/>
            <Tab.Screen name="Calendar" component={Calendar} options={{headerShown: false}}/>
            <Tab.Screen name="Profile" component={Profile} options={{headerShown: false}}/>
            
        </Tab.Navigator>
    )
}
