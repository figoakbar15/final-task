import React, { useEffect, useState, useContext } from 'react';
import { View, Text, Image, StyleSheet, Pressable, TouchableOpacity, FlatList, SafeAreaView, ScrollView} from 'react-native';
import AppLoading from 'expo-app-loading';
import * as Progress from 'react-native-progress';
import axios from 'axios';

import {BASE_URL,BASE_URL_AFTER_LOGIN} from '../config'

import {username, password} from '../Pages/Login'
import {date1} from '../Pages/Matakuliah'

import { AuthContext } from '../Auth/AuthContext';

import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_300Light,
  Poppins_300Light_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_500Medium,
  Poppins_500Medium_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
  Poppins_700Bold,
  Poppins_700Bold_Italic,
  Poppins_800ExtraBold,
  Poppins_800ExtraBold_Italic,
  Poppins_900Black,
  Poppins_900Black_Italic,
} from '@expo-google-fonts/poppins';


export default function Profile({route, navigation}){
    let [fontsLoaded] = useFonts({
        Poppins_100Thin,
        Poppins_100Thin_Italic,
        Poppins_200ExtraLight,
        Poppins_200ExtraLight_Italic,
        Poppins_300Light,
        Poppins_300Light_Italic,
        Poppins_400Regular,
        Poppins_400Regular_Italic,
        Poppins_500Medium,
        Poppins_500Medium_Italic,
        Poppins_600SemiBold,
        Poppins_600SemiBold_Italic,
        Poppins_700Bold,
        Poppins_700Bold_Italic,
        Poppins_800ExtraBold,
        Poppins_800ExtraBold_Italic,
        Poppins_900Black,
        Poppins_900Black_Italic,
      });

      const {userInfo, logout} = useContext(AuthContext);
      const [user, setuser]=useState({});
      const [items, setItems]=useState({});
      const [items1, setItems1]=useState({});

      
      
    const GetDataUser=()=>{
        axios.get(`${BASE_URL_AFTER_LOGIN}?wstoken=${userInfo.token}&wsfunction=core_webservice_get_site_info&moodlewsrestformat=json`)
        .then(res=>{
            const data = (res.data)
            setuser(data)
            axios.get(`${BASE_URL_AFTER_LOGIN}?wstoken=${userInfo.token}&wsfunction=core_enrol_get_users_courses&moodlewsrestformat=json&userid=${data.userid}`)
            .then(res=>{
                const data = (res.data)
                setItems(data)
            }).catch(err=>{
                console.log(`error ${e}`)

            })
        }).catch(err=>{
            console.log(`error ${e}`)

        })
    }

    const onSelectItem=(item)=>{
        return(
            navigation.navigate("Matakuliah",{
                screen:'Matakuliah',
                key: item,
            })
            )
    }

    const progressMatakuliah=(progress)=>{
        if(progress == null){
            return(
                progress = 0
            )
        } else{
            return(
                progress/100
            )
        }
    }

    
    useEffect(() => {
        GetDataUser()
    }, [])

    

    if (!fontsLoaded) {
        return <AppLoading />;
      }

    return(
        <View style={styles.container}>
            <View style={styles.profileContainer}>
                <Text style={styles.profileTitle}>
                    Profile
                </Text>
                <View style={styles.bioContainer}>
                    <Image 
                        style={styles.profilePic}
                        source={require('../Assets/profile.png')}
                    />
                    <View style={styles.profileData}>
                        <Text style={styles.name}>
                            {user.fullname}
                        </Text>
                        <Text style={styles.email}>
                            {user.username}
                        </Text>
                    </View>
                </View>
                <View style={styles.mataKuliahTitle}>
                    <Text style={styles.title}>Mata kuliah yang sedang diikuti</Text>
                </View>
                <SafeAreaView>
                    <FlatList
                        data={items}
                        keyExtractor={(item, index) => `${item.id}-${index}`}
                        renderItem={({item}) =>{
                        return (
                        <TouchableOpacity onPress={()=>onSelectItem(item)}>
                            <View style={styles.matakuliah}>
                                <View style={styles.containerMatakuliah}>
                                    <Text style={styles.namaMatakuliah}>
                                    {item.fullname}
                                    </Text>
                                    <Progress.Bar color='white' progress={progressMatakuliah(item.progress)} height={8} width={260} style={styles.progress}/>
                                    <Text style={styles.progressMatakuliah}>
                                        {item.progress.toFixed(1)} % Completed
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        )
                        }} />
                    </SafeAreaView>
                
            </View>
            <Pressable style={styles.logoutButton} onPress={() => logout()}>
                    <Text style={styles.logoutText}>Logout</Text>
            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        
    },

    profileContainer:{
        marginLeft: '10%',
        marginTop: '15%',
        marginRight: '10%',
        flex: 1
    },

    profileTitle:{
        fontFamily: 'Poppins_700Bold',
        marginTop: '15%',
        color: '#A02724',
        fontSize: 24,
    },

    bioContainer:{
        flexDirection: 'row',
        marginTop: '6%',
    },

    profileData:{
        marginTop: '10%',
        marginRight: '50%'
    },

    name:{
        fontSize: 24,
        fontFamily: 'Poppins_700Bold',
    },

    email:{
        fontFamily: 'Poppins_500Medium',
        fontSize: 15,
        color: '#757575'
    },

    mataKuliahTitle:{
        marginTop: '15%',
    },
    title:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 14,
        color: '#222222'
    },

    containerMatakuliah:{
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        marginTop:'5%',
        backgroundColor:'#A02724'
        
    },
    namaMatakuliah:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 14,
        color: 'white',
        marginLeft: '4%',
        marginRight: '4%',
        marginTop: '3%',
        marginBottom: '3%',
    },

    containerMatakuliah:{
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        marginTop:'5%',
        backgroundColor:'#A02724'
        
    },
    namaMatakuliah:{
        fontFamily: 'Poppins_700Bold',
        fontSize: 14,
        color: 'white',
        marginLeft: '4%',
        marginRight: '4%',
        marginTop: '3%',
        marginBottom: '3%',
    },

    progressMatakuliah:{
        color: 'white',
        alignSelf: 'flex-end',
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 14,
        marginRight: '5%',
        marginBottom: '3%'
    },

    progress:{
        marginLeft: '4%',
        marginBottom: '5%',
        borderColor: '#A02724',
    },

    logoutButton:{
        borderRadius: 20,
        padding: 8,
        elevation: 2,
        marginBottom: '5%',
        marginLeft: '10%',
        marginRight: '10%',
        bottom: 0,
        alignItems: 'center',
        backgroundColor: '#ff3333'
    },
    logoutText:{
        color: 'white',
        fontFamily: 'Poppins_700Bold',
    }
})